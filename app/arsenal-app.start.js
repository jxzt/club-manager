angular.module('Arsenal', [])
    .controller('MainCtrl', function($scope) {
        $scope.positions = [
		    {"id": 0, "title": "Goalkeepers"},
		    {"id": 1, "title": "Defenders"},
		    {"id": 2, "title": "Midfielders"},
		    {"id": 3, "title": "Forwards"}
        ];

        $scope.players = [
		    {"id":0, "nameF": "Petr", "nameL": "Cech", "url": "assets/img/cech.jpg", "position": "Goalkeepers", "squadNo": 33, "specPosition": "GK", "nationality" : "Czech Republic" },
		    {"id":1, "nameF": "Laurent", "nameL": "Koscielny", "url": "assets/img/koscielny.jpg", "position": "Defenders", "squadNo": 6, "specPosition": "CB", "nationality" : "France" },
		    {"id":2, "nameF": "Shkodran", "nameL": "Mustafi", "url": "assets/img/mustafi.jpg", "position": "Defenders", "squadNo": 20, "specPosition": "CB", "nationality" : "Germany" },
		    {"id":3, "nameF": "Hector", "nameL": "Bellerin", "url": "assets/img/bellerin.jpg", "position": "Defenders", "squadNo": 24, "specPosition": "RB", "nationality" : "Spain" },
		    {"id":4, "nameF": "Nacho", "nameL": "Monreal", "url": "assets/img/monreal.jpg", "position": "Defenders", "squadNo": 18, "specPosition": "LB", "nationality" : "Spain"  },
		    {"id":5, "nameF": "Santi", "nameL": "Cazorla", "url": "assets/img/cazorla.jpg", "position": "Midfielders", "squadNo": 19, "specPosition": "CM", "nationality" : "Spain"  },
		    {"id":6, "nameF": "Granit", "nameL": "Xhaka", "url": "assets/img/xhaka.jpg", "position": "Midfielders", "squadNo": 29, "specPosition": "CM", "nationality" : "Switzerland" },
		    {"id":7, "nameF": "Mesut", "nameL": "Ozil", "url": "assets/img/ozil.jpg", "position": "Midfielders", "squadNo": 11, "specPosition": "CAM", "nationality" : "Germany" },
		    {"id":8, "nameF": "Lucas", "nameL": "Perez", "url": "assets/img/perez.jpg", "position": "Forwards", "squadNo": 9, "specPosition": "RW", "nationality" : "Spain"  },
		    {"id":9, "nameF": "Alexis", "nameL": "Sanchez", "url": "assets/img/alexis.jpg", "position": "Forwards", "squadNo": 7, "specPosition": "ST", "nationality" : "Chile" },
		    {"id":10, "nameF": "Olivier", "nameL": "Giroud", "url": "assets/img/giroud.jpg", "position": "Forwards", "squadNo": 12, "specPosition": "ST", "nationality" : "France" },
		    {"id":11, "nameF": "Aaron", "nameL": "Ramsey", "url": "assets/img/ramsey.jpg", "position": "Midfielders", "squadNo": 8, "specPosition": "CM", "nationality" : "Wales" },
		    {"id":12, "nameF": "Alex", "nameL": "Iwobi", "url": "assets/img/iwobi.jpg", "position": "Forwards", "squadNo": 17, "specPosition": "LW", "nationality" : "Nigeria" },
		    {"id":13, "nameF": "Alex", "nameL": "Oxlade-Chamberlain", "url": "assets/img/ox.jpg", "position": "Midfielders", "squadNo": 15, "specPosition": "RW", "nationality" : "England" },
		    {"id":14, "nameF": "Theo", "nameL": "Walcott", "url": "assets/img/walcott.jpg", "position": "Forwards", "squadNo": 14, "specPosition": "RW", "nationality" : "England" },
		    {"id":15, "nameF": "David", "nameL": "Ospina", "url": "assets/img/ospina.jpg", "position": "Goalkeepers", "squadNo": 13, "specPosition": "GK", "nationality" : "Colombia" },
		    {"id":16, "nameF": "Mohammed", "nameL": "Elneny", "url": "assets/img/elneny.jpg", "position": "Midfielders", "squadNo": 35, "specPosition": "CM", "nationality" : "Egypt" },
		    {"id":17, "nameF": "Gabriel", "nameL": "Paulista", "url": "assets/img/gabriel.jpg", "position": "Defenders", "squadNo": 5, "specPosition": "CB", "nationality" : "Brazil" },
		    {"id":18, "nameF": "Kieran", "nameL": "Gibbs", "url": "assets/img/gibbs.jpg", "position": "Defenders", "squadNo": 3, "specPosition": "LB", "nationality" : "England"},
		    {"id":19, "nameF": "Francis", "nameL": "Coquelin", "url": "assets/img/coquelin.jpg", "position": "Midfielders", "squadNo": 34, "specPosition": "CDM", "nationality" : "France" },
		    {"id":20, "nameF": "Danny", "nameL": "Welbeck", "url": "assets/img/welbeck.jpg", "position": "Forwards", "squadNo": 23, "specPosition": "ST", "nationality" : "England" },
		    {"id":21, "nameF": "Rob", "nameL": "Holding", "url": "assets/img/holding.jpg", "position": "Defenders", "squadNo": 16, "specPosition": "CB", "nationality" : "England" },
		    {"id":22, "nameF": "Mathieu", "nameL": "Debuchy", "url": "assets/img/debuchy.jpg", "position": "Defenders", "squadNo": 2, "specPosition": "RB", "nationality" : "France" },
		    {"id":23, "nameF": "Jeff", "nameL": "Reine-Adelaide", "url": "assets/img/adelaide.jpg", "position": "Midfielders", "squadNo": 31, "specPosition": "CM", "nationality" : "France" },
		    {"id":24, "nameF": "Per", "nameL": "Mertesacker", "url": "assets/img/mertesacker.jpg", "position": "Defenders", "squadNo": 4, "specPosition": "CB", "nationality" : "Germany" },
		    {"id":25, "nameF": "Emiliano", "nameL": "Martinez", "url": "assets/img/martinez.jpg", "position": "Goalkeepers", "squadNo": 26, "specPosition": "GK", "nationality" : "Argentina" },
		    {"id":26, "nameF": "Jack", "nameL": "Wilshere", "url": "assets/img/wilshere.jpg", "position": "Midfielders", "squadNo": 10, "specPosition": "CM", "nationality" : "England" },
		    {"id":27, "nameF": "Wojciech", "nameL": "Szczesny", "url": "assets/img/szczesny.jpg", "position": "Goalkeepers", "squadNo": 1, "specPosition": "GK", "nationality" : "Poland" },
		    {"id":28, "nameF": "Calum", "nameL": "Chambers", "url": "assets/img/chambers.jpg", "position": "Defenders", "squadNo": 21, "specPosition": "CB", "nationality" : "England"  },
		    {"id":29, "nameF": "Joel", "nameL": "Campbell", "url": "assets/img/campbell.jpg", "position": "Forwards", "squadNo": 28, "specPosition": "RW", "nationality" : "Costa Rica" }
		];

		$scope.currentPosition = null;
		$scope.currentNation = null;
		$scope.sortType = null;
		$scope.playerSort = 'squadNo';
		$scope.buttonIconPlus = "showIcon"; //default
		$scope.buttonIconMinus = "hideIcon"; //default

		function setCurrentPosition(position) {
			$scope.currentPosition = position;

			cancelCreating();
			cancelEditing();
		}

		function closeModal() { //function to close modal upon completion
			$('#playerDetailsModal').modal('hide');
			$('#newPlayerModal').modal('hide');
		}

		// $scope.clearFilter = function() {
		// 	$scope.playerFilter = {};
		// }

		function clearFilter() {
			$scope.playerFilter = {};
			$scope.playerSort = 'squadNo';
		}

		// function setCurrentNation(nationality) {
		// 	$scope.currentNation = nationality;
			
		// 	cancelCreating();
		// 	cancelEditing();
		// }
		function changeListIcon() { //changes list icon from plus to minus
			if ($scope.buttonIconPlus === "hideIcon" ) {
				$scope.buttonIconMinus = "hideIcon";
				$scope.buttonIconPlus = "showIcon";
				
			} else if ($scope.buttonIconPlus === "showIcon") {
				$scope.buttonIconPlus = "hideIcon";
				$scope.buttonIconMinus = "showIcon";
			}
				
			
		}


		function isCurrentPosition(position) {
			return $scope.currentPosition !== null && position.title === $scope.currentPosition.title;
		}

		// function isCurrentNation(player) {
		// 	return $scope.currentNation !== null && player.nationality === $scope.currentNation.nationality;
		// }
		// $scope.isCurrentPosition = function(){
		// 	return $scope.currentPosition;
		// }; calls "isCurrentPosition" and it's state is returned to ng-class in html

		// var isCurrentPosition = function() {
		// 	return $scope.currentPosition;
		// }

		$scope.setCurrentPosition = setCurrentPosition; //makes public
		$scope.isCurrentPosition = isCurrentPosition;
		$scope.clearFilter = clearFilter; //writes clear filter to scope and makes public
		$scope.closeModal = closeModal;
		$scope.changeListIcon = changeListIcon;

		

		/* -------------------------------------------------------------------
		 * CRUD STUFF
		 * -----------------------------------------------------------------*/
		function resetCreateForm() {
			$scope.newPlayer = {
				nameF: '',
				nameL: '',
				url: '',
				squadNo: '',
				nationality: '',
				specPosition: '',
				position: '',
			}
		}

		function createPlayer(newPlayer) {
			//newPlayer.id = $scope.newPlayer.length;
			$scope.players.push(newPlayer); //pushes new player to array

			resetCreateForm();
		}    

		$scope.createPlayer = createPlayer;

		$scope.editedPlayer = null;

		// function setEditedPlayer(player) {
		// 	$scope.editedPlayer = player;
		// }

		function setEditedPlayer(player) {
			$scope.editedPlayer = angular.copy(player); //creates copy of bookmark for temporary editing
		}

		function updatePlayer(player) {
			var index = _.findIndex($scope.players, function(b) {
				return b.id == player.id
          });
          $scope.players[index] = player;

          $scope.editedPlayer = null;
          $scope.isEditing = false;
      	}

      	function isSelectedPlayer(playerId) { //if the player id matches, return true
      		return $scope.editedPlayer !== null && $scope.editedPlayer.id === playerId;
      	}

      	$scope.setEditedPlayer = setEditedPlayer;
      	$scope.updatePlayer = updatePlayer;
      	$scope.isSelectedPlayer = isSelectedPlayer;

      	function deletePlayer(player) {
          _.remove($scope.players, function (b) {
              return b.id == player.id;
          });
      }

      	$scope.deletePlayer = deletePlayer;

		/* -------------------------------------------------------------------
		 * Create & Edit States
		 * -----------------------------------------------------------------*/
		        
		$scope.isCreating = false; //default states
		$scope.isEditing = false;
		$scope.showPlayerDetails = false;
		$scope.selectedPlayer = "";
		

		function shouldShowCreating() {
			return $scope.isCreating && !$scope.isEditing;
		}

		function startCreating() {
			$scope.isCreating = true;
			$scope.isEditing = false;
			resetCreateForm();
		}

		function cancelCreating() {
			$scope.isCreating = false;
		}

		$scope.shouldShowCreating = shouldShowCreating;
		$scope.startCreating = startCreating;
		$scope.cancelCreating = cancelCreating;

		function shouldShowEditing() {
			shouldShowPlayerDetails();
          	return $scope.isEditing && !$scope.isCreating;
		}

		function startEditing() {
			$scope.isCreating = false;
			$scope.showPlayerDetails = false;
			$scope.isEditing = true;
		}

		function cancelEditing() {
			$scope.isEditing = false;
			$scope.editedPlayer = null;
		}

		function setPlayerDetails(player) {
			var index = _.findIndex($scope.players, function(b) {
				return b.id == player.id
          	});
          	cancelEditing();
          	cancelCreating();
          	$scope.showPlayerDetails = true;
          	return $scope.selectedPlayer = $scope.players[index];
		}

		function shouldShowPlayerDetails() {
			return $scope.showPlayerDetails;
			
		}		

		//writing functions to scope
		$scope.startEditing = startEditing;
		$scope.cancelEditing = cancelEditing;
		$scope.setPlayerDetails = setPlayerDetails;
		$scope.shouldShowEditing = shouldShowEditing;
		$scope.shouldShowPlayerDetails = shouldShowPlayerDetails;


    })
;
